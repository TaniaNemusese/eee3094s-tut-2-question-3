%Parameters
T = 273 ; % Temperature
P_0 = 101.3; % Initial Pressure
%P_h = P_0*exp(-h(t)/k); % Air Pressure
r_0 = 1; % initial radius

% densities
helium_density = 0.1785;
air_density = 1.225;

%A = pi* r^2; %cross sectional area
cd = 1; % drag coefficient
m = 1.5; % mass
g = 9.8; 
k = 6900; %scale height

%F_drag = 0.5*cd*air_density*A*(v^2);
V_0 = (4*pi)/3;
%V = V_0 * ((P_0+2)/(P_0*exp(-h/k)+2));
%r = (V_0 * ((P_0+2)/(P_0*exp(-h/k)+2)))^(1/3);
%m = 4.2105*V;

syms h(t)
[V]= odeToVectorField(m*diff(h,2)== 1.924*(V_0 * ((P_0+2)/(P_0*exp(-h/k)+2)))^(2/3)*(diff(h))^2 +10.266*(V_0 * ((P_0+2)/(P_0*exp(-h/k)+2)))^(1/3))-14.715;
M = matlabFunction(V,'vars', {'t','Y'});
sol = ode45(M,[0 20],[2 0]);
fplot(@(x)deval(sol,x,1), [0, 20]